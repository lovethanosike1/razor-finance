﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Login_Async
{
    public class Authentication
    {
        static DataBase dataBase = new DataBase();
        public static async Task ValidateUserAsync(string name, string password)
        {
            var queryUser = dataBase.Users().FirstOrDefault(reg => reg.Username == name && reg.Password == password);

            if (queryUser != null)
            {
                try 
                {
                    Task<Admin> getCredentialTaskAsync = GetCredentialsAsync(queryUser.Username);
                    var userDetails = await getCredentialTaskAsync;
                    if (userDetails != null)
                    {
                        Console.WriteLine($"DASHBOARD---------{userDetails.Username}-------------------\n" +
                            $"Last Visited:{userDetails.LastVisit}," +
                            $"Wallet Banlance:{userDetails.WalletBalance}\n" +
                            $"Next of Kin:{userDetails.NextOf_Kin}\n" +
                            $"Residential Address:{userDetails.ResidentialAddress}\n" +
                            $"BVN Status:{userDetails.BVNVerified}");

                    }
                    else
                    {
                        Console.WriteLine("Dear new User");
                        Console.WriteLine("Your Data has not be documented to the Admin" +
                            "(Details of new users are forwared to the admin after 7days)");
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                
            }
            else
            {
                Console.WriteLine("Invalid Userdetails.\nInput your the Username and Password");
                
            }

        }

        public static async Task<Admin> GetCredentialsAsync(string username)
        {
           return dataBase.RegisteredUsers().FirstOrDefault(d => d.Username == username);
        }
    }
}
