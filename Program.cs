﻿using System;
using System.Diagnostics;

namespace Login_Async
{
    class Program
    {
        static void Main(string[] args)
        {
            var timer1 = new Stopwatch(); // System.Diagnostics NameSpace
            timer1.Start();

            Application application = new Application();
            Application.Run();
            Console.WriteLine($"Mode of Operation: Synchronous\nTime Taken: {timer1.ElapsedMilliseconds} milliseconds");

            Console.WriteLine("-----------------------------------------------\n-----------------------------------------------");
            var timer2 = new Stopwatch();
        }
    }
}
