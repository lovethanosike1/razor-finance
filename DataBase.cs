﻿using System;
using System.Collections.Generic;


namespace Login_Async
{
    public class DataBase
    {
        public IEnumerable<Admin> RegisteredUsers()
        {
            return new List<Admin>()
            {
                new Admin(){Username = "adaobi78",Email="adaeje@gmail.com", WalletBalance=35960, ResidentialAddress="New Haven,Enugu",
                    LastVisit=DateTime.Parse("10/05/2021"),NextOf_Kin="Mary Ann",BVNVerified=true},

                new Admin(){Username = "joseph678",Email="joeken@yahoo.com", WalletBalance= 300000,ResidentialAddress="23 Chime,Enugu",
                    LastVisit=DateTime.Parse("10/07/2021"),NextOf_Kin="Saviour Okwudiri",BVNVerified=false},

                new Admin(){Username = "phils97",Email="p.chukwu@genesystechhhub.com", WalletBalance=15000,ResidentialAddress="Centenary,Enugu",
                    LastVisit=DateTime.Parse("02/07/2021"),NextOf_Kin="Evans Phillips",BVNVerified=true},

                new Admin(){Username = "sammyily001",Email="samuelkings@gmail.com", WalletBalance=25000, ResidentialAddress="New Haven Extension,Enugu",
                    LastVisit=DateTime.Parse("05/07/2021"),NextOf_Kin="Janeth Kings",BVNVerified=true },

                new Admin(){Username = "phils97",Email="p.chukwu@genesystechhhub.com", WalletBalance = 500000,ResidentialAddress="Centenary,Enugu",
                    LastVisit=DateTime.Parse("07/23/2021"),NextOf_Kin="Chukwu Chukwu",BVNVerified=false},

                new Admin(){Username = "frances93",Email="francessimon@yahoo.com",WalletBalance=10000, ResidentialAddress="Centenary,Enugu",
                    LastVisit=DateTime.Parse("01/08/2021"),NextOf_Kin="Chukwu Chukwu",BVNVerified=false}


            };

        }
        public IEnumerable<User> Users()
        {
            return new List<User>()
            {   new User(){Username = "adaobi78",Password="as0987t54#"},

                new User(){Username = "joseph678",Password="098765tre@"},

                new User(){Username = "phils97",Password="@Heavens"},

                new User(){Username = "sammyily001",Password="show@viber3"},

                new User(){Username = "frances93",Password="09@Lavsd7"},

                new User(){Username = "loveth21",Password="intime1"}
            };
        }
    }
    
}
