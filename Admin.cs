﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Login_Async
{
    public class Admin 
    {
        public string Username { get; set; }
        public string  Email { get; set; }
        public decimal WalletBalance { get; set; }
        public DateTime  LastVisit { get; set; }
        public string ResidentialAddress { get; set; }
        public string NextOf_Kin { get; set; }
        public bool BVNVerified { get; set; }
    }
}
